import React, { Component } from 'react';
import Ingredient from "./components/Ingredient/Ingredient";
import Burger from "./components/Burger/Burger";
import Price from './components/Price/Price';
import Cheese from './assets/cheese.jpg';
import Meat from './assets/meat.png';
import Salad from './assets/salad.svg';
import Bacon from './assets/bacon.png';

import './App.css';

class App extends Component {
  state = {
    ingredients: [
      {name: 'Meat', count: 0, price: 50, image: Meat},
      {name: 'Cheese', count: 0, price: 20, image: Cheese},
      {name: 'Salad', count: 0, price: 5, image: Salad},
      {name: 'Bacon', count: 0, price: 30, image: Bacon}
    ]
  };

  addIngr (event, index) {
    event.preventDefault();
    let ingredients = [...this.state.ingredients];
    let ingredient = {...ingredients[index]};
    ingredient.count++;
    ingredients[index] = ingredient;

    this.setState({ingredients});
  }

  addIngredientToBurger (classIng) {
    return (
      <div className={classIng}></div>
    );
  }
  getSumm () {
    let summ = 20;
    this.state.ingredients.map((price) => {
      summ += (price.count * price.price);
    });
    return summ;
  }

  removeQuant (event, index){
    event.preventDefault();
    let ingredients = [...this.state.ingredients];
    let ingredient = {...ingredients[index]};
    ingredient.count--;
    ingredients[index] = ingredient;

    this.setState({ingredients});

  }

  render() {
    return (
      <div className="App">
        <div className='ingredients'>
          {this.state.ingredients.map((numb, index) => {
            return <Ingredient onclick={event => this.addIngr(event, index)} img={numb.image} title={numb.name} quantity={numb.count} key={index} remove={event => this.removeQuant(event, index)}/>
          })}
        </div>
        <div className='burger-div'>
          <Burger>
            {this.state.ingredients.map((element) => {
              let items = [];
              for (let i = 0; i < element.count; i++) {
                items.push(this.addIngredientToBurger(element.name));
              }
              return items;
            })}
          </Burger>
          <Price priceOfBurger={this.getSumm()}/>
        </div>
      </div>
    );
  }
}

export default App;
