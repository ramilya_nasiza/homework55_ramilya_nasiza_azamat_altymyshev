import React from 'react';
import './Ingredient.css';

const Ingredient = props => (
  <div className="ingredient">
    <button onClick={props.onclick} className="ingredient-icon">
      <img src={props.img} alt=""/>
    </button>
    <h2>{props.title}</h2>
    <span>{'x ' + props.quantity}</span>
    <button onClick={props.remove} className={(props.quantity === 0) ? 'delete-hidden' : 'delete'}><i
        className="fas fa-trash-alt"></i></button>
  </div>
);

export default Ingredient;